import re
import string
import nltk
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from nltk.sentiment.vader import SentimentIntensityAnalyzer
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import precision_recall_fscore_support as score
from sklearn.model_selection import train_test_split




ds ="1429_1.csv"
df=pd.read_csv(ds)

df1_oasis = df.iloc[2816:3482,]
df2_fire_16gb = df.iloc[14448:15527,]
df3_paperwhite_4gb = df.iloc[17216:20392,]
df4_voyage = df.iloc[20392:20972,]
df5_paperwhite = df.iloc[20989:21019,]

print(df1_oasis.shape)
print(df2_fire_16gb.shape)
print(df3_paperwhite_4gb.shape)
print(df4_voyage.shape)
print(df5_paperwhite.shape)

# focus our attention on Kindle Paperwhite (dataframe3 and dataframe5), while saving other dataframes for later usage

df1_oasis.to_csv('Oasis.csv')
df2_fire_16gb.to_csv('Fire.csv')
df4_voyage.to_csv('Voyage.csv')

# combining df3 and df5 for Kindle Paperwhite Edition Data Only¶

frames = [df3_paperwhite_4gb,df5_paperwhite]
df4_voyage.to_csv('Voyage.csv')
kp = pd.concat(frames)
print(kp.head(5))
print(kp.tail(5))
kp = kp.reset_index()
print(kp.columns)
print(kp['reviews.rating'].describe())
kp.columns = ['Index','ID','Name','ASINS','Brand','Categories','Keys','Manufacturer','ReviewDate','ReviewDateAdded','ReviewDateSeen','PurchasedOnReview','RecommendStatus','ReviewID','ReviewHelpful','Rating','SourceURL','Comments','Title','UserCity','UserProvince','Username']
kp.columns
kp.head(5)
print(kp.columns.nunique())
kp = kp.drop(['ReviewID' , 'UserCity' , 'UserProvince','PurchasedOnReview'],axis = 1)
print(kp.columns.nunique())
print(kp.Rating.value_counts())
kp.Rating.value_counts()

# Data analysis: user rating distribution

kp.RecommendStatus.nunique()
kp.hist(column = 'Rating', by = 'RecommendStatus', color = 'Red')
plt.show()
print(kp.info())

# from the chart we can see that there are people with 4 and 5 score rating who still have not recommended the product

# slice for rating 5
# slice for recommended
# slice for comments
print("\n\n\n\n\n")
kp['Categories'] = 'Tablets'
kp['Name'] = 'Amazon Kindle Paperwhite'
print(kp.head(5))
print(kp.ReviewHelpful.value_counts())
print("\n\n\n\n\n")
# let's see the comments of people who have given a 5 star rating and have still not recommended the product
print(pd.DataFrame(kp[(kp.Rating==5)&(kp.RecommendStatus==False)]['Comments']))
print(kp.Username.nunique())
print(kp.shape)
sum(kp['Username'].value_counts()>1)



kp.head(2)
kp = kp.drop('Keys', axis = 1)
print(kp.columns.nunique())
kp = kp.reset_index()
print(kp.head(2))

# parsing ReviewDate, ReviewDateAdded and ReviewDateSeen to [Date and Time]

kp.ReviewDate = pd.to_datetime(kp['ReviewDate'], dayfirst=True)
kp.ReviewDateAdded = pd.to_datetime(kp['ReviewDateAdded'], dayfirst=True)
kp['ReviewDateSeen'] = kp['ReviewDateSeen'].str.split(',',expand = True).apply(lambda x:x.str.strip())
kp.ReviewDateSeen = pd.to_datetime(kp.ReviewDateSeen,dayfirst= True)
print(kp.head(4))
print("\n\n\n\n")
# likert scale analysis with Net Promoter Score: promoter-detractor/total

promoters = sum(kp.Rating==5)
passive = sum(kp.Rating == 4)
detractors = sum(np.logical_and(kp.Rating >= 1, kp.Rating <=3))
respondents = promoters+passive+detractors
NPS_P = ((promoters - detractors)/respondents )*100
print(NPS_P)
kp.plot(x = 'ReviewDate',y = 'Rating', kind = 'line',  figsize=(10,10))
plt.show()
print("\n\n\n\n")



# pivot table for promoter score by date
review_date = kp.ReviewDate
rating = kp.Rating
df_dr = pd.concat([review_date,rating],axis = 1)
print(df_dr.tail(5))
print(df_dr.shape)
df_dr = df_dr.groupby(['ReviewDate','Rating']).size().unstack(fill_value = 0)
print(df_dr.loc['2017-02-04'])
print(df_dr.head(5))

# So we now have rating distribution by date, let's now calculate the sum of ratings for 1,2 and 3 for each date,
# and finally add a new column to the df, while deleting 1,2,3
df_dr.columns = ['A','B','C','Passive','Promoters']
df_dr['Detractors'] = df_dr['A'] + df_dr['B'] + df_dr['C']
df_dr.head(5)
df_dr = df_dr.drop(labels = ['A','B','C'],axis = 1)
print(df_dr.head(5))
df_dr['NPS'] = (df_dr['Promoters'] - df_dr['Detractors']) * 100 / (df_dr['Passive'] + df_dr['Promoters'] + df_dr['Detractors'])
print(df_dr.head(5))
df_dr = df_dr.reset_index()
df_dr.plot( x = 'ReviewDate', y = 'NPS',kind = 'line', figsize=(10,10))
plt.show()

# Sentiment analysis - NLTK to find compound score
kp.Name.nunique()
kp.head(2)

# Columns to Remove : Index - ID - Name - ASINS - Brand - ReviewDateAdded - ReviewDateSeen - SourceURL
data =  kp.drop(['Index','ID','Name','ASINS','Brand','Categories','Manufacturer','ReviewDateAdded','ReviewDateSeen','SourceURL'], axis = 1)
# Cleaned Dataset
data = data.reset_index()
data.head(5)
data = data.drop(['ReviewDate'], axis = 1)

# Changing RecommendStatus from True/False to Recommend/Not Recommend
def status(data):
    if (data == True):
        data = "Recommend"
        return data
    else:
        data = "Not Recommend"
        return data


data['RecommendStatus'] = pd.DataFrame(data['RecommendStatus'].apply(lambda x: status(x)))
print(data.head(5))
dsa = data
dsa['feedback'] = dsa['Comments'] + dsa['Title']
dsa = dsa.drop(['Comments','Title'], axis = 1)



# Feature for Compound Score (using Sentiment Analysis)

sid = SentimentIntensityAnalyzer()

def polar_score(text):
    score=sid.polarity_scores(text)
    x = score['compound']
    return x


dsa['Compound_Score'] = dsa['feedback'].apply(lambda x : polar_score(x))

# Feature for Text Length

dsa['length'] = dsa['feedback'].apply(lambda x: len(x) - x.count(" "))
dsa.head(2)

# Understanding the Features added
# Ideally people who'll Not Recommend the product, would have a lot to say against
# the features of the product

bins = np.linspace(0,200,40)
plt.hist(dsa[dsa['RecommendStatus']=='Not Recommend']['length'], bins, alpha=0.5, normed=True, label='Not recommend')
plt.hist(dsa[dsa['RecommendStatus'] == 'Recommend']['length'],bins,alpha = 0.5,normed = True, label = 'Recommend')
plt.legend(loc = 'upper right')
plt.show()
# Above graph shows that our original hypothesis was correct


#Using TF-IDF and Random Forest to predict Recommendation Status

stopword =  nltk.corpus.stopwords.words('english')
ps = nltk.PorterStemmer()

def clean(text):
    no_punct = "".join([word.lower() for word in text if word not in string.punctuation])
    tokens = re.split('\W+',no_punct)
    text_stem = ([ps.stem(word) for word in tokens if word not in stopword])
    return text_stem

tf_idf = TfidfVectorizer(analyzer= clean)
xtf_idfvector = tf_idf.fit_transform(dsa['feedback'])

# New DataFrame having all the required features , the label we want to predict and the tf_idf vectorizer
Xfeatures_data = pd.concat([dsa['Compound_Score'], dsa['length'], pd.DataFrame(xtf_idfvector.toarray())], axis = 1)

X_train,X_test,y_train,y_test = train_test_split(Xfeatures_data, dsa['RecommendStatus'], test_size = 0.2)

rf = RandomForestClassifier(n_estimators= 50, max_depth= 20, n_jobs= -1)
rf_model = rf.fit(X_train,y_train)
sorted(zip(rf.feature_importances_,X_train.columns), reverse = True)[0:10]


# Applying Grid Search to change hyper parameters and then applying RF

def compute(n_est, depth):
    rf = RandomForestClassifier(n_estimators= n_est, max_depth= depth)
    rf_model = rf.fit(X_train, y_train)
    y_pred  = rf_model.predict(X_test)
    precision,recall,fscore,support  = score(y_test,y_pred, pos_label= 'Recommend', average = 'binary')
    print('Est: {}\ Depth: {}\ Precision: {}\ Recall: {}\ Accuracy: {}'.format(n_est, depth, round(precision,3), round(recall,3), (y_pred == y_test).sum()/ len(y_pred)))

for n_est in [10,30,50,70]:
    for depth in [20,40,60,80,90]:
        compute(n_est,depth)

